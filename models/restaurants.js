const mongoose = require('mongoose');
const Schema =mongoose.Schema;

const restaurantSchema = new Schema({
    restaurant_uuid: {
        type: String,
        required: true,
        unique: true
    },
    name: {
        type: String,
        required: true
    },
    country_code: {
        type: String,
        required: true
    }
}, {
    timestamps: true
});

var Restaurants = mongoose.model('Restaurants', restaurantSchema);
module.exports = Restaurants;