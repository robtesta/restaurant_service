const express = require('express');
const bodyParser = require('body-parser');
//const mongoose = require('mongoose');
const Restaurants = require('../models/restaurants');
const client = require('../config');
const images = require('../images');
const restaurantRouter= express.Router();
restaurantRouter.use(bodyParser.json());
var fs = require('file-system');

restaurantRouter.route('/')
.get((req,res,next) => {
    client.query(`SELECT a.restaurant_uuid, a.image_uuid, b.name, b.country_code, c.locales
    FROM restaurant_has_image a, restaurant b, country c
    WHERE a.restaurant_uuid=b.restaurant_uuid
    AND b.country_code=c.country_code`, (err,result)=>{

            const data = fs.readFileSync('./images.txt', 'utf8')
            console.log(data)
            var parsed = JSON.parse(data);
            var images = parsed.images;
            var restaurants=result.rows;
            var output= [];

            for (let i=0; i<restaurants.length; i++){
                var url_img =[];
                url_img.push("ciao");
                for (let j=0; j<images.length;j++){
                    if (restaurants[i].image_uuid==images[j].imageUuid)
                        url_img.push(images[j].url)
                }
                restaurants[i].images=url_img;
                if (restaurants[i].country_code=='FR')
                    restaurants[i].allowReview=true
                else
                    restaurants[i].allowReview=false
                
                
                console.log(restaurants[i]);
                output.push(restaurants[i]);
            }
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(output);


        })
    /*client.query(`select * from restaurant`, (err,result)=>{
        if (!err){
            var restaurants= result.rows;
            restaurants[0].prova=restaurants[0].name;
            console.log(restaurants);

            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(result.rows);
        }
        else{
            (err) => next(err);
        }
      })*/

})
.post((req, res, next) => {
    Restaurants.create(req.body)
    .then((restaurant) => {
        console.log('restaurant created', restaurant);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(restaurant);
    }, (err) => next(err))
    .catch((err) => next(err));})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /restaurants');
})
.delete((req, res, next) => {
    Restaurants.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
    .catch((err) => next(err));  
});

module.exports = restaurantRouter;