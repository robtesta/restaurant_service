const express = require('express');
const bodyParser = require('body-parser');
const client = require('../config');

const imageRouter= express.Router();
imageRouter.use(bodyParser.json());

imageRouter.route('/')
.get((req,res,next) => {
    client.query(`select * from restaurant_has_image`, (err,result)=>{
        if (!err){
            res.statusCode = 200;
            res.setHeader('Content-Type', 'application/json');
            res.json(result.rows);
        }
        else{
            (err) => next(err);
        }
      })
})
.post((req, res, next) => {
    res.end('Will add the image: ' + req.body.name + ' with details: ' + req.body.description);
})
.put((req, res, next) => {
    res.statusCode = 403;
    res.end('PUT operation not supported on /images');
})
.delete((req, res, next) => {
    res.end('Deleting all images');
});

module.exports = imageRouter;